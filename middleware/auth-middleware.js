const jwt = require("jsonwebtoken")
import { INTERNAL_SERVER_ERROR, UNAUTHORIZED } from 'http-status-codes'

var SECRET_KEY = "eMARTkEY";

export const GenerateToken = (user) => {

    try {
        let token = jwt.sign({ user_id: user._id }, SECRET_KEY);
        return token
    }

    catch (error) {
        console.log(error)
    }

}

export const AuthRequest = (req, res, next) => {

    const token = req.header("x-jwt-token");
    if (!token) {
        return res.status(UNAUTHORIZED).send({ Error: "Access denied" })
    }

    try {
        jwt.verify(token, SECRET_KEY);
        next()

    } catch (error) {
        return res.status(UNAUTHORIZED).send({ Error: "Invalid token" })
    }


}
