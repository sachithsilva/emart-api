const mongoose = require('mongoose')

const Schema = mongoose.Schema

const orderSchema = new Schema({
  //this should be changed as the items object in user model
  products: [
    {
      productId: {
        type: Schema.Types.ObjectId,
        ref: 'Product',
        required: true
      },
      quantity: { type: Number, required: true }
    }
  ],
  user: {
    userId: {
      type: Schema.Types.ObjectId,
      required: true,
      ref: 'User'
    }
  },
  shipping: {
    fname: { type: String },
    lname: { type: String },
    addrLine1: { type: String },
    addrLine2: { type: String },
    country: { type: String },
    city: { type: String },
    state: { type: String },
    zip: { type: String },
    shipping: { type: String, default: 'Free' }
  },
  payment: {
    name: { type: String },
    cardNo: { type: String },
    expireDate: { type: String },
    cvv: { type: String },
    total: { type: Number }
  }
})

module.exports = mongoose.model('Order', orderSchema)
