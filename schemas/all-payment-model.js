const mongoose = require('mongoose')

const Schema = mongoose.Schema

const allPaymentSchema = new Schema({

  _id: {
    type: Schema.Types.ObjectId,
    required: false
  },
  products: {
    length:{ type: Number }
  },  
  payment: {
    name: { type: String },
    total: { type: Number }
  }

})

module.exports = mongoose.model('Payments', allPaymentSchema)
