const mongoose = require('mongoose')
const Schema = mongoose.Schema

const currencySchema = new Schema({
    currencyCode: {
        type: String,
        required: true
    },
    currencyRate: {
        type: Number,
        required: true
    }
})

module.exports = mongoose.model('Currency', currencySchema)
