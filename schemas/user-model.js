const mongoose = require('mongoose')
const Schema = mongoose.Schema

const userSchema = new mongoose.Schema({

  email: { type: String, unique: true },
  password: { type: String, required: true },
  country: { type: String },
  status: { type: Boolean,  default: true },
  fname: { type: String },
  lname: { type: String },
  resetToken: String,
  resetTokenExpiration: Date,
  userType: { type: String, default: 'User' },
  cart: {
    items: [
      {
        productId: {
          type: Schema.Types.ObjectId,
          ref: 'Product',
          required: true
        },
        quantity: { type: Number, required: true }
      }
    ]
  }
})

const User = mongoose.model('User', userSchema)

module.exports = User
