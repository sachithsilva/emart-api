import { INTERNAL_SERVER_ERROR } from 'http-status-codes'
import to from 'await-to-js'
import { authUserModelValidation } from '../../../../shared/Validations'
import { GenerateToken } from '../../../../middleware/auth-middleware'

import { auth, authUser } from './auth-service'
/**
 * test api
 */

export const login = async (req, res) => {
  const { data } = req
  const [err, result] = await to(auth(data))

  if (result) {
    return res.send({
      data: { result }
    })
  }

  return res.status(INTERNAL_SERVER_ERROR).send({
    error: {
      message: `Error on test`,
      code: INTERNAL_SERVER_ERROR
    }
  })
}


export const authUserController = async (req, res) => {
  const { body } = req

  const { error } = authUserModelValidation.validate(body)

  if (!error) {
    const [err, user] = await to(authUser(body.email, body.password))

    if (user) {
      const token = GenerateToken(user)

      if (token) {
        return res.json({
          success: true,
          message: 'Authentication successful!',
          user: user,
          meta: token
        })
      }
      return res.status(INTERNAL_SERVER_ERROR).send({
        Error: `Something went wrong. Please try again later`
      })
    }
    return res.json({ Error: "Invalid credentials" })
  }
  if (error)
    return res.json({ Error: error.details[0].message })

  return res.json({ Error: 'Login Failed' })
}