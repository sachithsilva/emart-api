import to from 'await-to-js'
import bcrypt from 'bcryptjs'

import UserModel from '../../../../schemas/user-model'

export const auth = async (user, pass) => {
  response = auth.find(user, pass)
  if (response != null) {
    return 'sucesfull'
  }
  return 'unsucesfull'
}

export const authUser = async (email, pass) => {
  let [err, user] = await to(UserModel.findOne({ email: email }))

  let [comErr, result] = await to(bcrypt.compare(pass, user.password))
  console.log(user.status)
  if (user.status) {
    if (user && result) {
      return user
    }
  }

  return null
}
