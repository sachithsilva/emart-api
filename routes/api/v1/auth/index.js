import { Router } from 'express'
import { authUserController, createUserController } from './auth-controller'

const auth = Router()

auth.use((req, res, next) => {
  next()
})

auth.post('/',authUserController)

export default auth
