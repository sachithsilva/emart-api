import to from 'await-to-js'
import UserModel from '../../../../schemas/user-model'
import bcrypt from 'bcryptjs'

export const createUser = async newuser => {
  const response = await newuser.save()
  return response
}

export const getAllUsers = async () => {
  const userList = await UserModel.find({})
  if (userList) {
    return userList
  }
}

export const toggleUser = async (id, status) => {
  const response = await UserModel.updateOne({ _id: id }, { status: status })
  if (response) {
    return response
  }
}

export const updateUser = async (userId, newUser) => {

  if (newUser.password) {

    const [errUser, user] = await to(UserModel.findById(userId))

    const [err, oldHashedPassword] = await to(bcrypt.hash(newUser.oldPassword, 12))

    let [comErr, result] = await to(bcrypt.compare(newUser.oldPassword, user.password))

    if (result) {

      const [err, hashedPassword] = await to(bcrypt.hash(newUser.password, 12))

      if (hashedPassword) {
        await UserModel.updateOne(
          { _id: userId },
          {
            fname: newUser.fname,
            lname: newUser.lname,
            email: newUser.email,
            country: newUser.country,
            password: hashedPassword,
            // status: newUser.status
          })
      }
    }
    else {
      //Current Password is invalid
      return null
    }
  }

  await UserModel.updateOne(
    { _id: userId },
    {
      fname: newUser.fname,
      lname: newUser.lname,
      email: newUser.email,
      country: newUser.country,
      // status: newUser.status
    })

  return await to(UserModel.findById(userId))

}

export const getUser = async userId => {
  const [err, user] = await to(UserModel.findById(userId))
  if (user) {
    return user
  }
  return err
}
