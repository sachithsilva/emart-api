import { Router } from 'express'
import { AuthRequest } from '../../../../middleware/auth-middleware'


import {
  createUserController,
  getAllUserController,
  toggleUserStatusController,
  updateUserController,
  getUserDetailsController
} from './users-controller'

const user = Router()

user.get('/', AuthRequest, getAllUserController)

user.get('/:userId',AuthRequest, getUserDetailsController)

user.post('/', createUserController)

user.put('/',AuthRequest, toggleUserStatusController)

user.put('/:userId',AuthRequest, updateUserController)

export default user
