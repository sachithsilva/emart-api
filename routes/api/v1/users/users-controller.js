import {
  createUser,
  getAllUsers,
  toggleUser,
  updateUser,
  getUser
} from './users-service'
import {
  INTERNAL_SERVER_ERROR,
  NOT_ACCEPTABLE,
  OK,
  BAD_REQUEST
} from 'http-status-codes'
import bcrypt from 'bcryptjs'
import to from 'await-to-js'

import UserModel from '../../../../schemas/user-model'
import { userModelValidation, updateUserModelValidation } from '../../../../shared/Validations'
import { GenerateToken } from '../../../../middleware/auth-middleware'

export const createUserController = async (req, res) => {
  const { body } = req

  const { error } = userModelValidation.validate(body)

  if (!error) {
    const useremail = body.email
    const password = body.password
    const country = body.country
    const status = true
    const fname = body.fname
    const lname = body.lname

    const [err, hashedPassword] = await to(bcrypt.hash(password, 12))

    if (hashedPassword) {
      let newUser = new UserModel({
        email: useremail,
        password: hashedPassword,
        country: country,
        status: status,
        fname: fname,
        lname: lname
      })
      const response = await createUser(newUser)
      const token = GenerateToken(response)
      return res.json({
        user: response,
        meta: token
      })
    }
  }
  return res.status(NOT_ACCEPTABLE).json({ Error: error.details[0].message })

}

export const getAllUserController = async (req, res) => {
  let usersList = await getAllUsers()

  if (usersList) {
    return res.json(usersList)
  }
  return res.json({ Result: 'No Users found' })
}

export const toggleUserStatusController = async (req, res) => {
  const { body } = req
  //todo validate using joy

  const response = await toggleUser(body._id, body.status)
  if (response) {
    return res.json(response)
  }
}

export const updateUserController = async (req, res) => {

  if (req.body.password) {

    const { error } = userModelValidation.validate(req.body)


    if (error) {
      return res.status(NOT_ACCEPTABLE).json({
        error: {
          message: error.message,
          code: NOT_ACCEPTABLE
        }
      })
    }

    if (!error) {
      const response = await updateUser(req.params.userId, req.body)
      if (response) {
        return res.json(response)
      }
      return res.status(NOT_ACCEPTABLE).json({
        error: {
          message: "Current Password Is Invalid",
          code: NOT_ACCEPTABLE
        }
      })
    }

  }

  const { error } = updateUserModelValidation.validate(req.body)

  if (error) {
    return res.status(NOT_ACCEPTABLE).json({
      error: {
        message: error.message,
        code: NOT_ACCEPTABLE
      }
    })
  }

  if (!error) {
    const response = await updateUser(req.params.userId, req.body)
    if (response) {
      return res.json(response)
    }
    return res.status(NOT_ACCEPTABLE).json({
      error: {
        message: "Current Password Is Invalid",
        code: NOT_ACCEPTABLE
      }
    })
  }

  return res.status(NOT_ACCEPTABLE).json({
    error: {
      message: error.message,
      code: NOT_ACCEPTABLE
    }
  })

}

export const getUserDetailsController = async (req, res) => {
  const [err, user] = await to(getUser(req.params.userId))

  if (user) {
    return res.json({ data: { user }, code: OK })
  }

  return res.status(BAD_REQUEST).json({
    error: {
      message: 'No User Found',
      code: BAD_REQUEST
    }
  })
}
