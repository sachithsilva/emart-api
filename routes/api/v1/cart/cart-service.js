import to from 'await-to-js'

import UserModel from '../../../../schemas/user-model'

export const addItemToCart = async (userId, productId, quantity) => {

    let user = await UserModel.findById(userId)

    const existingItem = user.cart.items.find(item => item.productId == productId)

    if (existingItem) {
        let index = user.cart.items.indexOf(existingItem);
        user.cart.items[index].quantity = quantity
        return await to(await user.save())
    }

    user.cart.items.push({ productId: productId, quantity: quantity })
    return await to(await user.save())

}

export const getUserCart = async (userId) => {

    let user = await UserModel.findById(userId)

    await user.populate('cart.items.productId').execPopulate()

    return user.cart.items

}

export const removeItemFromCart = async (userId, productId) => {

    let user = await UserModel.findById(userId)
    const existingItem = user.cart.items.find(item => item.productId == productId)
    const itemIndex = user.cart.items.indexOf(existingItem);
    user.cart.items.splice(itemIndex, 1);
    return user.save()

}


export const removeItems = async (userId) => {

    let user = await UserModel.findById(userId)
    user.cart.items = [];
    return user.save()

}

