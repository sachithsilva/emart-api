import { INTERNAL_SERVER_ERROR } from 'http-status-codes'
import to from 'await-to-js'

import { addItemToCart, getUserCart, removeItemFromCart } from './cart-service'

export const addItemToCartController = async (req, res) => {
    const { body } = req

    const [err, response] = await to(addItemToCart(body.userId, body.productId, body.quantity))

    if (!response) {
        return res.json({
            success: true
        })
    }
    return res.json({ Error: err })

}


export const getUserCartController = async (req, res) => {
    const userId = req.params.userId

    const [err, response] = await to(getUserCart(userId))

    if (!err) {
        return res.json(response)
    }
    return res.json({ Error: err })

}


export const removeItemFromCartController = async (req, res) => {

    const [err, response] = await to(removeItemFromCart(req.params.userId, req.params.productId))

    if (response) {
        return res.json({
            success: true
        })
    }
    return res.json({ Error: err })

}