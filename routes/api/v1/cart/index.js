import { Router } from 'express'
import { addItemToCartController, getUserCartController, removeItemFromCartController } from './cart-controller'
import { AuthRequest } from '../../../../middleware/auth-middleware'

const cart = Router()

cart.use((req, res, next) => {
    next()
})

cart.get('/:userId', AuthRequest, getUserCartController)

cart.post('/', AuthRequest, addItemToCartController)

cart.delete('/:userId/:productId', AuthRequest, removeItemFromCartController)

export default cart

