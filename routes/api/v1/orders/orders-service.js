import OrderModel from '../../../../schemas/order-model'
// import getAllPayments from '../../../../schemas/all-payment-model'


export const getAllOrdersByUser = async (userId) => {
    const orderList = await OrderModel.find({'user.userId' : userId})
    if(orderList){
      return orderList;
    }
  }

  export const getAllPayments = async () => {
    const paymentList = await OrderModel.find({})
    if(paymentList){
      return paymentList;
    }
  }

export const createOrder = async (newOrder) => {
  const response = await newOrder.save()
  return response
}