import { Router } from 'express'
import { getAllOrdersController, createOrderController, getAllPaymentsOrderController } from './orders-controller'
import { AuthRequest } from '../../../../middleware/auth-middleware'


const order = Router()

order.get('/my-orders/:userId', AuthRequest, getAllOrdersController)

order.get('/', AuthRequest, getAllPaymentsOrderController)

order.post('/', AuthRequest, createOrderController)

export default order


