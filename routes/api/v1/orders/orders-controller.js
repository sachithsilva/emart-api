import { getAllOrdersByUser, createOrder, getAllPayments } from './orders-service'
import OrderModel from '../../../../schemas/order-model'
import { INTERNAL_SERVER_ERROR } from 'http-status-codes'
import { removeItems } from '../cart/cart-service'

export const getAllOrdersController = async (req, res) => {
  let userId = req.params.userId

  let orderList = await getAllOrdersByUser(userId);

  if (orderList) {
    return res.json(orderList)
  }

  return res.json({ Result: "No orders found" })
}

export const getAllPaymentsOrderController = async (req, res) => {

  let paymentDetails = await getAllPayments();
  let allPaymentsArry = []

  paymentDetails.map((order, i) => {
    allPaymentsArry.push({
      _id: order._id,
      id: i + 1,
      productCount: order.products.length,
      shippedTo: order.shipping.fname,
      totalAmount: order.payment.total
    })
  })

  if (allPaymentsArry) {
    return res.json(allPaymentsArry)
  }

  return res.json({ Result: "No Payments found" })
}
export const createOrderController = async (req, res) => {
  const { body } = req

  const products = body.products
  const user = body.user
  const shipping = body.shipping
  const payment = body.payment

  let newOrder = new OrderModel({
    products: products,
    user: user,
    shipping: shipping,
    payment: payment
  })

  await removeItems(user.userId)

  const response = await createOrder(newOrder)

  if (response) {
    return res.json({ Result: 'Success' })
  }
  return res.stasus(INTERNAL_SERVER_ERROR).json({ Error: 'Order is not created !' })


}