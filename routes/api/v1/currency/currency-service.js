import to from 'await-to-js'

import CurrencyModel from '../../../../schemas/currency-model'

export const getCurrencies = async () => {

    let currencies = await CurrencyModel.find({})

    return currencies

}

