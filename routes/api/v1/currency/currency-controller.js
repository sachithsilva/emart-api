import { INTERNAL_SERVER_ERROR } from 'http-status-codes'
import to from 'await-to-js'

import { getCurrencies, } from './currency-service'
import CurrencyModel from '../../../../schemas/currency-model'
import { GetCurrentExchangeRate } from '../../../../shared/CurrencyHandler'


export const getCurrenciesController = async (req, res) => {

    const [err, currencies] = await to(getCurrencies())

    if (currencies) {

        // currencies.map((currency, index) => {

        //     let newCurrencyRate = await GetCurrentExchangeRate(currency.currencyCode)
        //     await CurrencyModel.updateOne({ _id: currency._id }, { $set: { currencyRate: newCurrencyRate } })

        // })

    }

    if (!err) {
        return res.json(currencies)
    }
    return res.json({ Error: err })

}
