import { Router } from 'express'
import { getCurrenciesController } from './currency-controller'
import { AuthRequest } from '../../../../middleware/auth-middleware'


const currency = Router()

currency.get('/', getCurrenciesController)

export default currency