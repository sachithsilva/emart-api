import { Router } from 'express'

import { testx } from './test-controller'

const test = Router()

test.use((req, res, next) => {
  next()
})

test.get('/', testx)

export default test