import { INTERNAL_SERVER_ERROR } from 'http-status-codes'
import to from 'await-to-js'
import { Errors } from '../../../../shared/MessageHandler'

import { test } from './test-service'
/**
 * test api
 */

export const testx = async (req, res) => {
  const [err, result] = await to(test())

  if (result) {
    return res.send({
      message: Errors.INTERNAL_SERVER_ERROR,
      code: INTERNAL_SERVER_ERROR
    })
  }

  return res.status(INTERNAL_SERVER_ERROR).send({
    error: {
      message: Errors.INTERNAL_SERVER_ERROR,
      code: INTERNAL_SERVER_ERROR
    }
  })
}
