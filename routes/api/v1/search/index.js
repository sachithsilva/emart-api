import { Router } from 'express'

import { searchSProduct } from './search-controller'

const search = Router()

search.use((req, res, next) => {
  next()
})

search.post('/', searchSProduct)

export default search
