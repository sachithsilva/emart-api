import to from 'await-to-js'
import ProductModel from '../../../../schemas/product-model'

export const search = async ({ searchText }) => {
  let query = {
    $or: [
      {
        title: { $regex: `.*${searchText}.*`, $options: 'i' }
      },
      { description: { $regex: `.*${searchText}.*`, $options: 'i' } }
    ]
  }
  const [error, product] = await to(ProductModel.find(query))
  if (error) {
    return Promise.reject(error)
  }

  return { product }
}
