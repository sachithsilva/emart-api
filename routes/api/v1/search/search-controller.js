import { INTERNAL_SERVER_ERROR } from 'http-status-codes'
import to from 'await-to-js'

import { search } from './search-service'

// eslint-disable-next-line import/prefer-default-export
export const searchSProduct = async (req, res) => {
  const [err, result] = await to(search(req.body))

  if (result) {
    return res.json(result.product)
  }

  return res.status(INTERNAL_SERVER_ERROR).send({
    error: {
      message: `Error searching for product. Query: ${req.body}`,
      code: INTERNAL_SERVER_ERROR
    }
  })
}
