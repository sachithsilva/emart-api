//import to from 'await-to-js'
import ProductModel from '../../../../schemas/product-model'

// export const getAllProducts = async () => {
//   const [err, product] = await to(ProductModel.find({}))
//   if (product) {
//     return product
//   }
//   return err
// }

export const getAllProducts = async () => {
  const productList = await ProductModel.find({})
  if (productList) {
    return productList
  }
  // .then(productList => {
  //   return productList
  // })
  // .catch(err => {
  //   console.log(err)
  //   return null
  // })
}

export const getProductByDetalsById = async productId => {
  const productDetail = await ProductModel.findById(productId)

  if (productDetail) {
    return productDetail
  }
}

export const addProduct = async newProduct => {
  const createdProduct = await newProduct.save()
  if (createdProduct) {
    return createdProduct
  }
}

export const updateProduct = async editProduct => {
  const createdProduct = await editProduct.updateOne(editProduct)
  if (createdProduct) {
    return true
  }
}

export const deleteProduct = async productId => {
  const filter = { _id: productId }
  const productDetail = await ProductModel.deleteOne(filter)

  if (productDetail) {
    return productDetail
  }
}
