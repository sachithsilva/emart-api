import {
  INTERNAL_SERVER_ERROR,
  NOT_ACCEPTABLE,
  OK,
  BAD_REQUEST
} from 'http-status-codes'
import to from 'await-to-js'
//import fs from './'

import ProductModel from '../../../../schemas/product-model'
import azureHandler from '../../../../shared/AzureFileHandler'
import { productModelValidation } from '../../../../shared/Validations'
import { GetExchangeRate } from '../../../../shared/CurrencyHandler'

import {
  getAllProducts,
  addProduct,
  getProductByDetalsById,
  updateProduct,
  deleteProduct
} from './products-service'
import { response } from 'express'
/**
 * products api
 */

export const createProductController = async (req, res) => {
  const { body } = req

  const { error } = productModelValidation.validate(body)
  if (!error) {
    let newProduct = new ProductModel({
      title: body.title,
      price: body.price,
      description: body.description,
      imageUrl: body.imageUrl,
      fullDescription: body.fullDescription
    })

    const [err, response] = await to(addProduct(newProduct))

    if (response) {
      return res.json(response)
    }
    return res.status(INTERNAL_SERVER_ERROR).json({
      error: {
        message: 'Unable create product',
        code: INTERNAL_SERVER_ERROR
      }
    })
  }
  return res.status(NOT_ACCEPTABLE).json({
    error: {
      message: error.message,
      code: NOT_ACCEPTABLE
    }
  })
}

export const getallProductsController = async (req, res) => {
  const [metaErr, currencyMeta] = await to(GetExchangeRate(req, res))

  let [err, productList] = await to(getAllProducts())

  if (productList) {
    return res.json({ products: productList, meta: currencyMeta })
  }
  return res.status(INTERNAL_SERVER_ERROR).json({
    error: {
      message: 'No Product Found',
      code: INTERNAL_SERVER_ERROR
    }
  })
}

export const getProductByIdController = async (req, res) => {
  let productId = req.params.productId

  const [err, productDetails] = await to(getProductByDetalsById(productId))

  if (productDetails) {
    return res.json({ data: { productDetails }, code: OK })
  }
  return res.status(INTERNAL_SERVER_ERROR).json({
    error: {
      message: 'No Product Found',
      code: INTERNAL_SERVER_ERROR
    }
  })
}

export const uploadImageProductsController = (req, res) => {
  if (req.file) {
    azureHandler.imageUpload(
      req.file.filename, // Generated filename
      req.file.path, // Local server path for reading the file
      'product-pic', // Folder to save in Azure Storage
      function(azure) {
        console.log('ddd', azure.fileUrl)
        res.json({ fileurl: azure.fileUrl })
      }
    )
  } else {
    res.status(500).json({ status: 0 })
  }
}

export const putProductController = async (req, res) => {
  const { body } = req
  const { error } = productModelValidation.validate(body)
  if (!error) {
    let editProduct = new ProductModel({
      _id: body._id,
      title: body.title,
      price: body.price,
      description: body.description,
      imageUrl: body.imageUrl,
      fullDescription: body.fullDescription
    })

    const [err, response] = await to(updateProduct(editProduct))

    if (response) {
      return res.json({ data: { response }, code: OK })
    }
    return res.status(BAD_REQUEST).json({
      error: {
        message: 'Product Updating operation failed',
        code: BAD_REQUEST
      }
    })
  }
  return res.status(BAD_REQUEST).json({
    error: {
      message: error.message,
      code: BAD_REQUEST
    }
  })
}

export const deleteProductByIdController = async (req, res) => {
  let productId = req.params.productId

  let productDetails = await deleteProduct(productId)

  if (productDetails) {
    return res.json(productDetails)
  }
  return res.json({ Result: 'No product found' })
}
