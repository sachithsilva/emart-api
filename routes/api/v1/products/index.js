import { Router } from 'express'
import { path } from 'path'
import multer from 'multer'
import { AuthRequest } from '../../../../middleware/auth-middleware'

import {
  createProductController,
  getallProductsController,
  uploadImageProductsController,
  getProductByIdController,
  putProductController,
  deleteProductByIdController
} from './products-controller'

const product = Router()

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'temp')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})

const upload = multer({
  storage: storage,
  limits: { fileSize: 10000000, files: 1 }
})

product.use((req, res, next) => {
  next()
})

product.post('/upload', AuthRequest, upload.single('file'), uploadImageProductsController)

product.get('/', getallProductsController)

product.get('/:productId', getProductByIdController)

product.post('/', AuthRequest, createProductController)

product.put('/', AuthRequest, putProductController)

product.delete('/:productId', AuthRequest, deleteProductByIdController)

export default product
