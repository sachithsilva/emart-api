setup prettier for code formatting:
(https://www.robinwieruch.de/prettier-eslint)
npm install -g prettier eslint
then install the Prettier and ESLint extension

start server:
npm run server

debugging mode:
npm run dev

test end point api call:
http://localhost:3001/api/v1/test
