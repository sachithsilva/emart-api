import express from 'express'
import cors from 'cors'

import { dbInit } from './shared/DbHandler'
import routes from './routes'

require('dotenv').config()

const app = express()

// database
dbInit()

// middleware
app.use(express.json())
app.use(cors())

routes(app)

//Dev
app.listen(process.env.API_PORT, () =>
  console.log(`server started on port: ${process.env.API_PORT}`)
)

// //Prod
// app.listen(
//   process.env.PORT, () => console.log(`server started on port: ${process.env.PORT}`)
// )