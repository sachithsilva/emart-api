import AWS from 'aws-sdk'
import fs from 'fs'

AWS.config.update({
  accessKeyId: 'ASIAXAXG2RTGVEJ7XBX2',
  secretAccessKey: 'ux6xYzfAHzWof8Ezn4h2aiUjL6leU9PHcEvA5WLq'
})

var imageUpload = (filename, filepath, directory, mimeType, callback) => {
  console.log('REC: ', filename, filepath, directory)
  // Read in the file, convert it to base64, store to S3
  fs.readFile(filepath, function(err, data) {
    if (err) {
      console.log(err)
    }

    var base64data = Buffer.from(data, 'binary')

    var s3 = new AWS.S3()
    s3.putObject(
      {
        Bucket: 'e-mart',
        Key: directory + '/' + filename,
        Body: base64data,
        ContentType: mimeType,
        ACL: 'public-read'
      },
      function(resp) {
        console.log(resp)

        callback({
          fileUrl:
            'https://e-mart.s3.amazonaws.com/' + directory + '/' + filename
        })

        // Delete the temporary created file.
        fs.unlink(filepath, err => {
          if (err) throw err
        })
      }
    )
  })
}

exports.imageUpload = imageUpload
