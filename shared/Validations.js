const Joi = require('@hapi/joi')

export const productModelValidation = Joi.object({
  title: Joi.string()
    .required()
    .min(3)
    .max(30),

  price: Joi.number().required(),

  description: Joi.string(),
  imageUrl: Joi.string(),
  fullDescription: Joi.string().allow(''),
  _id: Joi.string()
})

export const authUserModelValidation = Joi.object({
  email: Joi.string()
    .required()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),

  password: Joi.string().required()
})

export const userModelValidation = Joi.object({

  fname: Joi.string().required().min(3).max(20),
  lname: Joi.string().required().min(3).max(20),

  email: Joi.string()
    .required()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),

  password: Joi.string().required().min(6),
  country: Joi.string().required(),
  oldPassword: Joi.string(),
  _id: Joi.string()

})



export const updateUserModelValidation = Joi.object({

  fname: Joi.string().required().min(3).max(20),
  lname: Joi.string().required().min(3).max(20),

  email: Joi.string()
    .required()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
  country: Joi.string().required(),
  _id: Joi.string()

})


