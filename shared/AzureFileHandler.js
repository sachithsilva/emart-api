import {
  Aborter,
  BlobURL,
  BlockBlobURL,
  ContainerURL,
  ServiceURL,
  StorageURL,
  SharedKeyCredential,
  uploadStreamToBlockBlob
} from '@azure/storage-blob'
import fs from 'fs'
const getStream = require('into-stream')
// const { promisify } = require('util')
// const readFile = promisify(fs.readFile)
require('dotenv').config()

const imageUpload = (filename, filepath, directory, callback) => {
  const containerName = 'products'
  const ONE_MEGABYTE = 1024 * 1024
  const uploadOptions = { bufferSize: 4 * ONE_MEGABYTE, maxBuffers: 20 }
  const ONE_MINUTE = 60 * 1000
  const aborter = Aborter.timeout(30 * ONE_MINUTE)

  const sharedKeyCredential = new SharedKeyCredential(
    process.env.AZURE_STORAGE_ACCOUNT_NAME,
    process.env.AZURE_STORAGE_ACCOUNT_ACCESS_KEY
  )
  const pipeline = StorageURL.newPipeline(sharedKeyCredential)
  const serviceURL = new ServiceURL(
    `https://${process.env.AZURE_STORAGE_ACCOUNT_NAME}.blob.core.windows.net`,
    pipeline
  )

  console.log('REC: ', filename, filepath, directory)
  // Read in the file, convert it to base64

  fs.readFile(filepath, function(err, data) {
    try {
      if (err) {
        console.log(err)
      }

      var base64data = Buffer.from(data, 'binary')

      const blobName = filename
      const stream = getStream(base64data)
      const containerURL = ContainerURL.fromServiceURL(
        serviceURL,
        containerName
      )
      const blobURL = BlobURL.fromContainerURL(containerURL, blobName)
      const blockBlobURL = BlockBlobURL.fromBlobURL(blobURL)

      uploadStreamToBlockBlob(
        aborter,
        stream,
        blockBlobURL,
        uploadOptions.bufferSize,
        uploadOptions.maxBuffers
      )
      callback({
        fileUrl: blobURL.url
      })
      //Delete the temporary created file.
      fs.unlink(filepath, err => {
        if (err) throw err
      })
      console.log('File uploaded to Azure Blob storage. :' + blobURL.url)
    } catch (err) {
      console.log('Something went wrong.' + err)
      fs.unlink(filepath, err => {
        if (err) throw err
      })
    }
  })
}

exports.imageUpload = imageUpload
