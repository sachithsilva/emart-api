import axios from 'axios'
import CurrencyModel from '../schemas/currency-model'


export const GetExchangeRate = async (req, res) => {

    try {

        let ipAddress = req.connection.remoteAddress
        ipAddress = "112.134.104.174"

        // const ipData = await axios.get(`https://ipapi.co/${ipAddress}/json/`);

        // const currencyCode = ipData.data.currency
        const currencyCode = "LKR"

        const currencyEntry = await CurrencyModel.findOne({ currencyCode: currencyCode })

        if (!currencyEntry) {

            const CurrencyData = await axios.get(`https://free.currconv.com/api/v7/convert?q=USD_${currencyCode}&compact=ultra&apiKey=464afdc1e7784453b7a4`)

            let newExchangeRate = new CurrencyModel({
                currencyCode: currencyCode,
                currencyRate: CurrencyData.data[`USD_${currencyCode}`]
            })

            await newExchangeRate.save()
            return newExchangeRate

        }

        return currencyEntry

    }

    catch (error) {
        console.log(error)
    }


}


export const GetCurrentExchangeRate = async (currencyCode) => {

    try {

        const CurrencyData = await axios.get(`https://free.currconv.com/api/v7/convert?q=USD_${currencyCode}&compact=ultra&apiKey=464afdc1e7784453b7a4`)

        if (CurrencyData.data[`USD_${currencyCode}`]) {
            return CurrencyData.data[`USD_${currencyCode}`]
        }
        return null
    }

    catch (error) {
        console.log(error)
    }


}


